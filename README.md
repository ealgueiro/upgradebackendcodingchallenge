# Reservations API Service

Start application with:
mvn spring-boot:run

For testing, run ReservationsTestClient.java as Java application.


API
Endpoint:
http://localhost:8080/ReservationsApi/api/ 

Get all reservations:
GET http://localhost:8080/ReservationsApi/api/reservation/

Get single reservation:
GET http://localhost:8080/ReservationsApi/api/reservation/1

Create reservation:
POST http://localhost:8080/ReservationsApi/api/reservation/
header: Content-type application/json
body:
{
	"fullName": "Sarah Zaraza",
	"email": "sarah@server.com",
	"startDate": 10000,
	"endDate": 20000
}

Update reservation:
PUT http://localhost:8080/ReservationsApi/api/reservation/5
header: Content-type application/json
body:
{
	"fullName": "Tom Tom",
	"email": "tom@server.com",
	"startDate": 40000,
	"endDate": 50000
}

Delete single reservation:
DELETE http://localhost:8080/ReservationsApi/api/reservation/1

Delete all reservations:
DELETE http://localhost:8080/ReservationsApi/api/reservation/
