package com.upgrade.springboot;
 
import java.net.URI;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.upgrade.springboot.model.Reservation;
import com.upgrade.springboot.util.DateUtil;

import org.springframework.web.client.RestTemplate;
 
/**
 * RestTemplate provides higher level methods that correspond to each of the six main HTTP methods
 * that make invoking many RESTful services a one-liner and enforce REST best practices.
 */
public class ReservationsTestClient {
 
    public static final String REST_SERVICE_URI = "http://localhost:8080/ReservationsApi/api";
     
    /* GET */
    @SuppressWarnings("unchecked")
    private static void listAllReservations(){
        System.out.println("Testing listAllReservations API-----------");
         
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> reservationsMap = restTemplate.getForObject(REST_SERVICE_URI+"/reservation/", List.class);
         
        if(reservationsMap!=null){
            for(LinkedHashMap<String, Object> map : reservationsMap){
                System.out.println("Reservation : id="+map.get("id")+", Full Name="+map.get("fullName")+", Email="+map.get("email")+", StartDate="+map.get("startDate")+", EndDate="+map.get("endDate"));
            }
        }else{
            System.out.println("No reservation exist----------");
        }
    }
     
    /* GET */
    private static void getReservation(){
        System.out.println("Testing getReservation API----------");
        RestTemplate restTemplate = new RestTemplate();
        Reservation reservation = restTemplate.getForObject(REST_SERVICE_URI+"/reservation/1", Reservation.class);
        System.out.println(reservation);
    }
     
    /* POST */
    private static void createReservation() {
        System.out.println("Testing create Reservation API----------");
        RestTemplate restTemplate = new RestTemplate();
        Reservation reservation = new Reservation(0, "User Zero", "user0@server.com", DateUtil.addDaysToDate(new Date(), 1), DateUtil.addDaysToDate(new Date(), 4));
        
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI+"/reservation/", reservation, Reservation.class);
        System.out.println("Location : "+uri.toASCIIString());
    }
 
    /* PUT */
    private static void updateReservation() {
        System.out.println("Testing update Reservation API----------");
        RestTemplate restTemplate = new RestTemplate();
        Reservation reservation  = new Reservation(1, "User One", "user1@server.com", DateUtil.addDaysToDate(new Date(), 5), DateUtil.addDaysToDate(new Date(), 8));
        restTemplate.put(REST_SERVICE_URI+"/reservation/1", reservation);
        System.out.println(reservation);
    }
 
    /* DELETE */
    private static void deleteReservation() {
        System.out.println("Testing delete Reservation API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/reservation/3");
    }
 
 
    /* DELETE */
    private static void deleteAllReservations() {
        System.out.println("Testing all delete Reservations API----------");
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/reservation/");
    }
 
    public static void main(String args[]){
        listAllReservations();
        getReservation();
        createReservation();
        listAllReservations();
        updateReservation();
        listAllReservations();
        deleteReservation();
        listAllReservations();
        deleteAllReservations();
        listAllReservations();
    }
}