package com.upgrade.springboot.model;

import java.util.Date;

public class Reservation {

	private long id;

	private String fullName;

	private String email;

	private Date startDate;

	private Date endDate;

	public Reservation() {
		id = 0;
	}

	public Reservation(long id, String fullName, String email, Date startDate, Date endDate) {
		super();
		this.id = id;
		this.fullName = fullName;
		this.email = email;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservation other = (Reservation) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Reservation [id=" + id + ", fullName=" + fullName + ", email=" + email + ", startDate=" + startDate + ", endDate=" + endDate + "]";
	}

}