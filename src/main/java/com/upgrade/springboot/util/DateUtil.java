package com.upgrade.springboot.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static String dateFormat = "yyyy-MM-dd";
	
	public static String addDaysToDateAsString(String dateAsString, int days) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Calendar c = Calendar.getInstance();
		c.setTime(sdf.parse(dateAsString));
		c.add(Calendar.DATE, days);
		return sdf.format(c.getTime());
	}
	
	public static Date addDaysToDate(Date date, int days) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		c.add(Calendar.DATE, days);
		return c.getTime();
	}

	public static String dateToString (Date date){
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return sdf.format(c.getTime());
	}
	
	public static Date stringToDate (String dateAsString) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(sdf.parse(dateAsString));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return c.getTime();
	}
}
