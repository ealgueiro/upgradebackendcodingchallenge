package com.upgrade.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;


@SpringBootApplication(scanBasePackages={"com.upgrade.springboot"})// same as @Configuration @EnableAutoConfiguration @ComponentScan combined
@EnableAsync //enable asynchronous method calls in Spring Boot, for performance
public class ReservationsApiApp {

	public static void main(String[] args) {
		SpringApplication.run(ReservationsApiApp.class, args);
	}
}
