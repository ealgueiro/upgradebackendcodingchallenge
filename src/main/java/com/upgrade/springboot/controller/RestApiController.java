package com.upgrade.springboot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.upgrade.springboot.model.Reservation;
import com.upgrade.springboot.service.ReservationService;
import com.upgrade.springboot.util.CustomErrorType;

@RestController
@RequestMapping("/api")
public class RestApiController {

	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);

	@Autowired
	ReservationService reservationService; //Service which will do all data retrieval/manipulation work

	// -------------------Retrieve All Reservations---------------------------------------------

	@RequestMapping(value = "/reservation/", method = RequestMethod.GET)
	public ResponseEntity<List<Reservation>> listAllReservations() {
		List<Reservation> reservations = reservationService.findAllReservations();
		if (reservations.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<Reservation>>(reservations, HttpStatus.OK);
	}

	// -------------------Retrieve Single Reservation------------------------------------------

	@RequestMapping(value = "/reservation/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getReservation(@PathVariable("id") long id) {
		logger.info("Fetching Reservation with id {}", id);
		Reservation reservation = reservationService.findById(id);
		if (reservation == null) {
			logger.error("Reservation with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Reservation with id " + id 
					+ " not found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Reservation>(reservation, HttpStatus.OK);
	}

	// -------------------Create a Reservation-------------------------------------------

	@RequestMapping(value = "/reservation/", method = RequestMethod.POST)
	public ResponseEntity<?> createReservation(@RequestBody Reservation reservation, UriComponentsBuilder ucBuilder) {
		logger.info("Creating Reservation : {}", reservation);

		if (reservationService.isReservationExist(reservation)) {
			logger.error("Unable to create. A Reservation with full name {} already exists", reservation.getFullName());
			return new ResponseEntity(new CustomErrorType("Unable to create. A Reservation with full name " + 
			reservation.getFullName() + " already exists."),HttpStatus.CONFLICT);
		}
		reservationService.saveReservation(reservation);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/reservation/{id}").buildAndExpand(reservation.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ------------------- Update a Reservation ------------------------------------------------

	@RequestMapping(value = "/reservation/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateReservation(@PathVariable("id") long id, @RequestBody Reservation reservation) {
		logger.info("Updating Reservation with id {}", id);

		Reservation currentReservation = reservationService.findById(id);

		if (currentReservation == null) {
			logger.error("Unable to update. Reservation with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to upate. Reservation with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		currentReservation.setFullName(reservation.getFullName());
		currentReservation.setEmail(reservation.getEmail());
		currentReservation.setStartDate(reservation.getStartDate());
		currentReservation.setEndDate(reservation.getEndDate());

		reservationService.updateReservation(currentReservation);
		return new ResponseEntity<Reservation>(currentReservation, HttpStatus.OK);
	}

	// ------------------- Delete a Reservation-----------------------------------------

	@RequestMapping(value = "/reservation/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteReservation(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting Reservation with id {}", id);

		Reservation reservation = reservationService.findById(id);
		if (reservation == null) {
			logger.error("Unable to delete. Reservation with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("Unable to delete. Reservation with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		reservationService.deleteReservationById(id);
		return new ResponseEntity<Reservation>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Delete All Reservations-----------------------------

	@RequestMapping(value = "/reservation/", method = RequestMethod.DELETE)
	public ResponseEntity<Reservation> deleteAllReservations() {
		logger.info("Deleting All Reservations");

		reservationService.deleteAllReservations();
		return new ResponseEntity<Reservation>(HttpStatus.NO_CONTENT);
	}

}