package com.upgrade.springboot.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import com.upgrade.springboot.model.Reservation;

import com.upgrade.springboot.util.DateUtil;


@Service("reservationService")
public class ReservationServiceImpl implements ReservationService{
	
	private static final AtomicLong counter = new AtomicLong();
	
	private static List<Reservation> reservations;
	
	static{
		reservations= populateDummyReservations();
	}

	public List<Reservation> findAllReservations() {
		return reservations;
	}
	
	public Reservation findById(long id) {
		for(Reservation reservation : reservations){
			if(reservation.getId() == id){
				return reservation;
			}
		}
		return null;
	}
	
	public Reservation findByFullName(String fullName) {
		for(Reservation reservation : reservations){
			if(reservation.getFullName().equalsIgnoreCase(fullName)){
				return reservation;
			}
		}
		return null;
	}
	
	public void saveReservation(Reservation reservation) {
		reservation.setId(counter.incrementAndGet());
		reservations.add(reservation);
	}

	public void updateReservation(Reservation reservation) {
		int index = reservations.indexOf(reservation);
		reservations.set(index, reservation);
	}

	public void deleteReservationById(long id) {
		
		for (Iterator<Reservation> iterator = reservations.iterator(); iterator.hasNext(); ) {
		    Reservation reservation = iterator.next();
		    if (reservation.getId() == id) {
		        iterator.remove();
		    }
		}
	}

	public boolean isReservationExist(Reservation reservation) {
		return findByFullName(reservation.getFullName())!=null;
		//return findById(reservation.getId())!=null;
	}
	
	public void deleteAllReservations(){
		reservations.clear();
	}

	private static List<Reservation> populateDummyReservations(){
		List<Reservation> reservations = new ArrayList<Reservation>();
		//long id, String fullName, String email, Date startDate, Date endDate
		reservations.add(new Reservation(counter.incrementAndGet(),"User One", "user1@server.com", DateUtil.addDaysToDate(new Date(), 1), DateUtil.addDaysToDate(new Date(), 4)));
		reservations.add(new Reservation(counter.incrementAndGet(),"User Two", "user2@server.com", DateUtil.addDaysToDate(new Date(), 5), DateUtil.addDaysToDate(new Date(), 8)));
		reservations.add(new Reservation(counter.incrementAndGet(),"User Three", "user3@server.com", DateUtil.addDaysToDate(new Date(), 9), DateUtil.addDaysToDate(new Date(), 12)));
		reservations.add(new Reservation(counter.incrementAndGet(),"User Four", "user4@server.com", DateUtil.addDaysToDate(new Date(), 13), DateUtil.addDaysToDate(new Date(), 16)));
		return reservations;
	}

}
