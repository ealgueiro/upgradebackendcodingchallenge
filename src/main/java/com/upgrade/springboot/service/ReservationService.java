package com.upgrade.springboot.service;


import java.util.List;

import com.upgrade.springboot.model.Reservation;

public interface ReservationService {
	
	Reservation findById(long id);
	
	Reservation findByFullName(String fullName);
	
	void saveReservation(Reservation reservation);
	
	void updateReservation(Reservation reservation);
	
	void deleteReservationById(long id);

	List<Reservation> findAllReservations();
	
	void deleteAllReservations();
	
	boolean isReservationExist(Reservation reservation);
	
}
